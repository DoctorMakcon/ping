#include "IcmpHeader.h"

using namespace std;

list<int>* queue = new list<int>();

char* GetDestinationAddress(int argc, char** argv);
bool Ping(char* destAddr);
unsigned short CalculateCheckSum(unsigned short* icmpHeader, int packetSize);
timeval SetTimeout();
void ShowStatistic();

int main(int argc, char* argv[])
{
	WSADATA wsaData;
	if(WSAStartup(MAKEWORD(2, 0), &wsaData) == 0)
	{
		if(LOBYTE(&wsaData, wVersion) >= 2)
		{
			char* str = GetDestinationAddress(argc, argv);
			printf("Ping with %s\n", str);
			for(int i = 0; i < 4; i++)
			{
				if(Ping(str))
					Sleep(1000);
			}
			if(queue->size() > 0)
				ShowStatistic();
		}
		else printf("Version error\n");
		WSACleanup();
		queue = NULL;
		delete queue;
		getch();
	}
	else printf("Initialize error\n");
}

char* GetDestinationAddress(int argc, char** argv)
{
	if(argc < 2)
	{
		printf("You don't enter destination address. Try to ping localhost\n");
		return "127.0.0.1";
	}
	else
	{
		in_addr addr;
		char* address = argv[1];
		hostent* host;
		if(address[0] > 47 && address[0] < 58)
			return address;
		else
		{
			host = gethostbyname(address);
			if(!host)
			{
				printf("Incorrect host name. Try to ping localhost\n");
				return "127.0.0.1";
			}
			else
			{
				addr.s_addr = *(u_long*)host->h_addr_list[0];
				address = inet_ntoa(addr);
				return address;
			}
		}
	}
}

bool Ping(char* destAddr)
{
	int hSocket = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);

	struct ECHOREQUEST echoReq;
	echoReq.icmpHeader.icmp_type = ICMP_ECHO;
	echoReq.icmpHeader.icmp_code = ICMP_ECHO_REPLY;
	echoReq.icmpHeader.icmp_id = GetCurrentProcessId();
	echoReq.icmpHeader.icmp_seq = 0;
	echoReq.time = GetTickCount();
	echoReq.icmpHeader.icmp_checksum = NULL;
	FillMemory(echoReq.data, 64, 80);
	echoReq.icmpHeader.icmp_checksum = CalculateCheckSum((unsigned short*)&echoReq, sizeof(ECHOREQUEST));

	SOCKADDR_IN	sockAddr;
	sockAddr.sin_family = AF_INET;
	sockAddr.sin_addr.s_addr = inet_addr(destAddr);
	sockAddr.sin_port = 0;

	if(echoReq.icmpHeader.icmp_checksum != 0)
	{
		int sendBytes = sendto(hSocket, (char*)&echoReq, sizeof(ECHOREQUEST), 0, (sockaddr*)&sockAddr, sizeof(sockAddr));
		if(sendBytes == -1)
		{
			printf("sendto() error\n");
			return false;
		}
	}

	fd_set sockSet;
	FD_ZERO(&sockSet);
	FD_SET(hSocket, &sockSet);
	timeval timeout = SetTimeout();
	if(select(hSocket + 1, &sockSet, NULL, NULL, &timeout) == 0)
	{
		printf("Request timeout\n");
		return false;
	}
	else
	{
		ECHOREPLY echoReply;
		SOCKADDR_IN recvAddr;
		int recvAddrSize = sizeof(recvAddr);
		int recvBytes = recvfrom(hSocket, (char*)&echoReply, sizeof(ECHOREPLY), 0, (sockaddr*)&recvAddr, &recvAddrSize);
		if(recvBytes == -1)
		{
			printf("recvfrom() error\n");
			return false;
		}
		int time = GetTickCount() - echoReply.echoRequest.time;
		if(echoReply.echoRequest.icmpHeader.icmp_type == ICMP_ECHO_REPLY)
		{
			if(echoReply.echoRequest.icmpHeader.icmp_id == GetCurrentProcessId())
			{
				printf("Answer from %s time = %d ms\n", destAddr, time);
				queue->push_back(time);
				closesocket(hSocket);
				return true;
			}
			else 
			{
				printf("Received packet belongs to another process\n");
				closesocket(hSocket);
				return false;
			}
		}
		else
		{
			printf("Received packet wasn't echo reply for ping\n");
			closesocket(hSocket);
			return false;
		}
	}
}

unsigned short CalculateCheckSum(unsigned short* icmpHeader, int packetSize)
{
	unsigned int Sum = 0;
	unsigned short result;
	
	while(packetSize > 1)
	{
		Sum += *icmpHeader++;
		packetSize -= 2;
	}
	if(packetSize == 1)
		Sum += *(unsigned char*)icmpHeader;

	Sum = (Sum >> 16) + (Sum & 0xFFFF);
	Sum += (Sum >> 16);
	result = ~Sum;
	return result;
}

timeval SetTimeout()
{
	timeval timeout;
	timeout.tv_sec = 1;
	timeout.tv_usec = 0;
	return timeout;
}

void ShowStatistic()
{
	printf("Packets : sent = 4, received = %d, lost = %d\n", queue->size(), (4 - queue->size()));
	int average = 0, min = *(queue->begin()), max = *(queue->begin());
	for(list<int>::iterator i = queue->begin(); i != queue->end(); i++)
	{
		average += *i;
		if(min > *i)
			min = *i;
		if(max < *i)
			max = *i;
	}
	average /= queue->size();
	printf("Time : average = %d ms, minimum = %d ms, maximum = %d ms\n", average, min, max);
}