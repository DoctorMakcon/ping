#include <iostream>
#include <winSock2.h>
#include <conio.h>
#include <windows.h>
#include <list>

#define ICMP_ECHO 8
#define ICMP_ECHO_REPLY 0
#define	ICMP_HEADER_SIZE 8

struct Icmp
{
	unsigned char icmp_type;							    //type 8 & code 0 -> echo message
	unsigned char icmp_code;
	unsigned short icmp_checksum;
	unsigned short icmp_id;									//pid
	unsigned short icmp_seq;								//multiple pings
	char icmp_data;
};

struct ECHOREQUEST
{
	struct Icmp icmpHeader;
	int time;
	char data[64];
};

struct Ip
{
	unsigned char ip_verlen;
	unsigned char ip_tos;
	unsigned short ip_len;
	unsigned short ip_id;
	unsigned short flagoff;
	unsigned char ip_ttl;
	unsigned char ip_proto;
	unsigned short ip_checksum;
	struct in_addr ip_src_addr;
	struct in_addr ip_dest_addr;
};

struct ECHOREPLY
{
	struct Ip ipHeader;
	ECHOREQUEST echoRequest;
	char Filler[256];
};